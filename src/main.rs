use std::convert::Infallible;
use std::env;
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};

use bytes::Bytes;
use diesel::sql_types::{Integer, Text, Bigint};
use http_body_util::{BodyExt, Full};
use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper::{Method, Request, Response, StatusCode};
use tokio::net::{TcpListener, TcpStream};

use hyper_util::rt::{TokioIo, TokioTimer};

use diesel::{prelude::*, QueryableByName};
use tokenserver_db::params::{PostNode, PostService};

#[derive(serde::Deserialize)]
struct InitRequest {
    pub domain: String,
    #[serde(rename = "userLimit")]
    pub user_limit: u64,
}

// An async function that consumes a request, does nothing with it and returns a
// response.
async fn hello(
    req: Request<impl hyper::body::Body>,
    backend: &str,
    is_initialized: &Arc<Mutex<bool>>,
) -> Result<Response<Full<Bytes>>, Infallible> {
    match req.uri().path() {
        "/" | "/index.html" => {
            if *is_initialized.lock().unwrap() == false {
                return Ok(Response::builder()
                    .status(StatusCode::TEMPORARY_REDIRECT)
                    .header("Location", "/init")
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            return Ok(Response::new(Full::new(Bytes::from(
                include_bytes!("./static/index.html").as_ref(),
            ))));
        }
        "/android" | "/android/" | "/android/index.html" => {
            if *is_initialized.lock().unwrap() == false {
                return Ok(Response::builder()
                    .status(StatusCode::TEMPORARY_REDIRECT)
                    .header("Location", "/init")
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            return Ok(Response::new(Full::new(Bytes::from(
                include_bytes!("./static/android.html").as_ref(),
            ))));
        }
        "/ios" | "/ios/" | "/ios/index.html" => {
            if *is_initialized.lock().unwrap() == false {
                return Ok(Response::builder()
                    .status(StatusCode::TEMPORARY_REDIRECT)
                    .header("Location", "/init")
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            return Ok(Response::new(Full::new(Bytes::from(
                include_bytes!("./static/ios.html").as_ref(),
            ))));
        }
        "/desktop" | "/desktop/" | "/desktop/index.html" => {
            if *is_initialized.lock().unwrap() == false {
                return Ok(Response::builder()
                    .status(StatusCode::TEMPORARY_REDIRECT)
                    .header("Location", "/init")
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            return Ok(Response::new(Full::new(Bytes::from(
                include_bytes!("./static/desktop.html").as_ref(),
            ))));
        }
        "/favicon.ico" => {
            // Return 404
            return Ok(Response::builder()
                .status(404)
                .body(Full::new(Bytes::from("Not Found")))
                .unwrap());
        }
        "/init" | "/init/" | "/init/index.html" => {
            if *is_initialized.lock().unwrap() == true {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            if req.method() == Method::GET {
                return Ok(Response::builder()
                    .body(Full::new(Bytes::from(include_str!("static/init.html"))))
                    .unwrap());
            } else if req.method() != Method::POST {
                return Ok(Response::builder()
                    .status(StatusCode::METHOD_NOT_ALLOWED)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            // Parse body as JSON
            let Ok(body) = req.into_body().collect().await else {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            };
            let body_bytes = body.to_bytes();
            let Ok(init_request) = serde_json::from_slice::<'_, InitRequest>(&body_bytes) else {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            };
            let db_connection = MysqlConnection::establish(&env::var("DATABASE_URL").unwrap())
                .expect("Error connecting to database");

            let new_service = PostService {
                service: "sync-1.5".to_string(),
                pattern: "{node}/1.5/{uid}".to_string(),
            };

            if let Err(e) =
                diesel::sql_query("INSERT INTO services (service, pattern) VALUES (?, ?)")
                    .bind::<Text, _>(&new_service.service)
                    .bind::<Text, _>(&new_service.pattern)
                    .execute(&db_connection)
            {
                eprintln!("Error inserting service: {:?}", e);
                return Ok(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            let Ok(id) = diesel::sql_query("SELECT LAST_INSERT_ID() AS id")
                .get_result::<tokenserver_db::results::LastInsertId>(&db_connection)
            else {
                return Ok(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            };
            // We do not have table information from diesel
            let new_node = PostNode {
                service_id: id.id as i32,
                node: init_request.domain,
                available: 1,
                current_load: 0,
                capacity: init_request.user_limit as i32,
                downed: 0,
                backoff: 0,
            };
            const QUERY: &str = "INSERT INTO nodes (service, node, available, current_load, capacity, downed, backoff) VALUES (?, ?, ?, ?, ?, ?, ?)";
            if let Err(e) = diesel::sql_query(QUERY)
                .bind::<Integer, _>(new_node.service_id)
                .bind::<Text, _>(&new_node.node)
                .bind::<Integer, _>(new_node.available)
                .bind::<Integer, _>(new_node.current_load)
                .bind::<Integer, _>(new_node.capacity)
                .bind::<Integer, _>(new_node.downed)
                .bind::<Integer, _>(new_node.backoff)
                .execute(&db_connection)
            {
                eprintln!("Error inserting node: {:?}", e);
                return Ok(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Full::new(Bytes::new()))
                    .unwrap());
            }
            *is_initialized.lock().unwrap() = true;
            return Ok(Response::builder()
                .status(StatusCode::NO_CONTENT)
                .body(Full::new(Bytes::new()))
                .unwrap());
        }
        _ => {
            let Ok(stream) = TcpStream::connect(backend).await else {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_GATEWAY)
                    .body(Full::new(Bytes::from("Bad Gateway")))
                    .unwrap());
            };
            let io = TokioIo::new(stream);
            let Ok((mut sender, conn)) = hyper::client::conn::http1::handshake(io).await else {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_GATEWAY)
                    .body(Full::new(Bytes::from("Bad Gateway")))
                    .unwrap());
            };
            tokio::task::spawn(async move {
                if let Err(err) = conn.await {
                    println!("Connection failed: {:?}", err);
                }
            });
            let mut req_builder = hyper::Request::builder()
                .method(req.method())
                .uri(req.uri())
                .version(req.version());
            for (key, value) in req.headers() {
                req_builder = req_builder.header(key, value);
            }
            let Ok(collected_body) = req.into_body().collect().await else {
                return Ok(Response::builder()
                    .status(StatusCode::BAD_GATEWAY)
                    .body(Full::new(Bytes::from("Bad Gateway")))
                    .unwrap());
            };
            let req = req_builder
                .body(Full::new(collected_body.to_bytes()))
                .unwrap();
            let resp = sender.send_request(req).await;
            match resp {
                Ok(resp) => {
                    let (parts, body) = resp.into_parts();
                    let mut resp_builder = hyper::Response::builder().status(parts.status);
                    for (key, value) in parts.headers {
                        if let Some(key) = key {
                            resp_builder = resp_builder.header(key, value);
                        }
                    }
                    let Ok(full_body) = body.collect().await else {
                        return Ok(Response::builder()
                            .status(StatusCode::BAD_GATEWAY)
                            .body(Full::new(Bytes::from("Bad Gateway")))
                            .unwrap());
                    };

                    let resp = resp_builder.body(Full::new(full_body.to_bytes())).unwrap();
                    return Ok(resp);
                }
                Err(err) => {
                    println!("Error sending request: {:?}", err);
                    return Ok(Response::builder()
                        .status(StatusCode::BAD_GATEWAY)
                        .body(Full::new(Bytes::from("Bad Gateway")))
                        .unwrap());
                }
            }
        }
    }
}

#[derive(Debug, Default, Eq, PartialEq, QueryableByName)]
struct CountResult {
    #[sql_type = "Bigint"]
    count: i64,
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let backend_path = std::env::var("BACKEND").unwrap_or_else(|_| "localhost:8080".to_string());
    let addr: SocketAddr = ([0, 0, 0, 0], 3000).into();
    let is_initialized = Arc::new(Mutex::new(false));
    
    {
        let db_connection = MysqlConnection::establish(&env::var("DATABASE_URL").unwrap())
            .expect("Error connecting to database");
        let Ok(count) = diesel::sql_query("SELECT COUNT(*) AS count FROM services") 
            .get_result::<CountResult>(&db_connection) else {
            eprintln!("Error checking if database is initialized");
            return Ok(());
        };
        if count.count > 0 {
            *is_initialized.lock().unwrap() = true;
        }
    }

    // Bind to the port and listen for incoming TCP connections
    let listener = TcpListener::bind(addr).await?;
    println!("Listening on http://{}", addr);
    loop {
        // When an incoming TCP connection is received grab a TCP stream for
        // client<->server communication.
        //
        // Note, this is a .await point, this loop will loop forever but is not a busy loop. The
        // .await point allows the Tokio runtime to pull the task off of the thread until the task
        // has work to do. In this case, a connection arrives on the port we are listening on and
        // the task is woken up, at which point the task is then put back on a thread, and is
        // driven forward by the runtime, eventually yielding a TCP stream.
        let (tcp, _) = listener.accept().await?;
        // Use an adapter to access something implementing `tokio::io` traits as if they implement
        // `hyper::rt` IO traits.
        let io = TokioIo::new(tcp);

        // Spin up a new task in Tokio so we can continue to listen for new TCP connection on the
        // current task without waiting for the processing of the HTTP1 connection we just received
        // to finish
        let backend_path = backend_path.clone();
        let is_initialized = is_initialized.clone();
        tokio::task::spawn(async move {
            // Handle the connection from the client using HTTP1 and pass any
            // HTTP requests received on that connection to the `hello` function
            if let Err(err) = http1::Builder::new()
                .timer(TokioTimer::new())
                .serve_connection(
                    io,
                    service_fn(|req| hello(req, &backend_path, &is_initialized)),
                )
                .await
            {
                println!("Error serving connection: {:?}", err);
            }
        });
    }
}
