FROM rust:1.78.0-slim as rust-builder

RUN cargo install cargo-chef

FROM rust-builder as planner

WORKDIR /app
COPY . /app

RUN cargo chef prepare --recipe-path recipe.json

FROM rust-builder as build-env

RUN apt update && apt install -y libmariadb-dev-compat

WORKDIR /app

COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

COPY . /app

RUN cargo build --release --bin=ff-sync

FROM debian:bookworm-slim

RUN apt update && apt install -y libmariadb3 && rm -rf /var/lib/apt/lists/* && apt clean

COPY --from=build-env /app/target/release/ff-sync /

CMD ["/ff-sync"]
